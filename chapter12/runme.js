function Coffee(roast, ounces) {
    this.roast = roast;
    this.ounces = ounces;
    this.getSize = function () {
        if (this.ounces == 8) {
            return "small";
        } else if (this.ounces == 12) {
            return "medium";
        } else {
            return "large";
        }
    };
    this.toStr = function () {
        return "You've ordered " +
    }
}

var houseBlend = new Coffee("House Blend", 12);
console.log(houseBlend.toStr());

var darkRoast = new Coffee("Dark Roast", 16);
console.log(darkRoast.toStr());
